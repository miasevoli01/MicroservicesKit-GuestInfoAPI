#!/usr/bin/env bash

PROJECT_ROOT="$(git rev-parse --show-toplevel)"

"${PROJECT_ROOT}/tools/build/commands/build.sh"
"${PROJECT_ROOT}/tools/build/commands/run.sh"
