# Build System

The build system is responsible for building the products
of this project, which is a bundled copy of the openapi
specification.

```bash
commands/build.sh
```

This builds and runs `tools/build` which generates `build/openapi.yaml`.
For more information see [build's documentation](../../tools/build/README.md)
